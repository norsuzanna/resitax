/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Home = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView />
      <View style={styles.overallContainer}>
        <LinearGradient
          start={{x: 0.0, y: 0.0}}
          end={{x: 1, y: 0.9}}
          locations={[0.1, 0.5, 1]}
          colors={['#78D1F0', '#9EB7FF', '#7966FF']}
          style={styles.upperContainer}>
          <View style={styles.userView}>
            <View style={styles.imageView}>
              <Image source={require('./images/user_colored.png')} />
            </View>
            <View style={styles.textView}>
              <Text style={styles.welcomeText}>Welcome Syazlyeen</Text>
              <Text style={styles.incomeTaxText}>SG 000000000</Text>
            </View>
          </View>
        </LinearGradient>
        <View style={styles.lowerContainer}>
          <Text style={styles.snapText}>Snap and Stores Receipts</Text>
          <View style={styles.cardView}>
            <TouchableOpacity style={styles.cardButton} underlayColor="#fff">
              <Image
                style={styles.cardImage}
                source={require('./images/receipt.png')}
              />
              <Text style={styles.cardText}>Receipts</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cardButton} underlayColor="#fff">
              <Image
                style={styles.cardImage}
                source={require('./images/tax_claim.png')}
              />
              <Text style={styles.cardText}>Tax Claim 2019</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.cardView}>
            <TouchableOpacity style={styles.cardButton} underlayColor="#fff">
              <Image
                style={styles.cardImage}
                source={require('./images/income.png')}
              />
              <Text style={styles.cardText}>Income</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cardButton} underlayColor="#fff">
              <Image
                style={styles.cardImage}
                source={require('./images/spending.png')}
              />
              <Text style={styles.cardText}>Current Spending</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  overallContainer: {
    height: '100%',
  },
  upperContainer: {
    height: '20%',
    width: '100%',
    paddingBottom: '2%',
  },
  userView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: '10%',
  },
  imageView: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#7966FF',
    borderWidth: 3,
  },
  textView: {marginLeft: '5%'},
  welcomeText: {color: 'white', fontWeight: 'bold', fontSize: 20},
  incomeTaxText: {color: 'white', fontWeight: 'bold'},
  lowerContainer: {
    flex: 1,
    height: '90%',
    borderRadius: 20,
    backgroundColor: 'white',
    zIndex: 1,
    position: 'absolute',
    top: '17%',
    width: '100%',
    paddingTop: '10%',
  },
  snapText: {
    fontWeight: 'bold',
    fontSize: 25,
    marginRight: '15%',
    marginLeft: '15%',
    textAlign: 'center',
  },
  cardView: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    marginTop: '10%',
  },
  cardButton: {
    borderRadius: 25,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'white',
    shadowColor: '#ccc',
    shadowRadius: 10,
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 1,
    width: 120,
    height: 120,
  },
  cardImage: {
    width: '45%',
    height: '20%',
    flex: 1,
    alignSelf: 'center',
    alignContent: 'center',
  },
  cardText: {
    flex: 1,
    alignSelf: 'center',
    alignContent: 'center',
    fontSize: 15,
    marginTop: '5%',
    textAlign: 'center',
  },
});

export default Home;
