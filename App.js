import * as React from 'react';
import {View, StyleSheet, Dimensions, Image, SafeAreaView} from 'react-native';
import {TabView, TabBar, SceneMap} from 'react-native-tab-view';
import Home from './Home';
import Icon from 'react-native-vector-icons/AntDesign';
Icon.loadFont();

const AnalyticsRoute = () => <View style={styles.analyticsView} />;
const EFilingRoute = () => <View style={styles.efilingView} />;
const CameraRoute = () => <View style={styles.cameraView} />;
const ProfileRoute = () => <View style={styles.profileView} />;

export default class App extends React.Component {
  state = {
    index: 0,
    routes: [
      {key: 'home', icon: 'home', uri: require('./images/home.png')},
      {
        key: 'analytics',
        icon: 'piechart',
        uri: require('./images/chart.png'),
      },
      {key: 'camera', icon: 'camerao', uri: require('./images/camera.png')},
      {key: 'efiling', icon: 'file1', uri: require('./images/document.png')},
      {key: 'profile', icon: 'user', uri: require('./images/user_white.png')},
    ],
  };

  _renderIcon = ({route, color}) => (
    <Icon name={route.icon} size={24} color={color} />
  );

  _renderImage = ({route, color}) => <Image source={route.uri} />;

  render() {
    return (
      <>
        <SafeAreaView />
        <TabView
          tabBarPosition="bottom"
          navigationState={this.state}
          renderScene={SceneMap({
            home: Home,
            analytics: AnalyticsRoute,
            camera: CameraRoute,
            efiling: EFilingRoute,
            profile: ProfileRoute,
          })}
          onIndexChange={index => this.setState({index})}
          initialLayout={{width: Dimensions.get('window').width}}
          renderTabBar={props => (
            <TabBar
              {...props}
              getLabelText={({route: {title}}) => title}
              indicatorStyle={styles.indicator}
              tabStyle={styles.tabStyle}
              style={styles.tab}
              renderIcon={this._renderImage}
            />
          )}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  analyticsView: {
    flex: 1,
    backgroundColor: '#F1EEFC',
  },
  cameraView: {flex: 1, backgroundColor: '#78D1F0'},
  efilingView: {flex: 1, backgroundColor: '#9EB7FF'},
  profileView: {
    flex: 1,
    backgroundColor: '#F79155',
  },
  tabStyle: {
    backgroundColor: 'white',
    color: '#7966FF',
  },
  tab: {
    backgroundColor: 'white',
    color: '#7966FF',
  },
  indicator: {
    backgroundColor: '#7966FF',
    color: '#7966FF',
  },
});
